
#include "Controller.hpp"
#include "Controller_p.hpp"

#include <unordered_map>

#ifdef __linux__
#include <linux/input.h>
#include <sys/inotify.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#else
#	error "FIXME"
#endif

#include <Flat/Utils/IdGenerator.hpp>
#include <Flat/Input/Fd.hpp>
#include <Flat/Input/EventDispatcher.hpp>

#include "Debug.hpp"




namespace Flat {
namespace Controller {




static constexpr char kDevInputPath[] = "/dev/input";




#if 0
ButtonEvent::ButtonEvent( ButtonEventPrivate * const d ) :
	QEvent( static_cast<QEvent::Type>( Event_Button ) ),
	d_( d )
{
}


ButtonType ButtonEvent::buttonType() const
{
	return d_->buttonType;
}


bool ButtonEvent::isDown() const
{
	return d_->isDown;
}




AxisEvent::AxisEvent( AxisEventPrivate * const d ) :
	QEvent( static_cast<QEvent::Type>( Event_Axis ) ),
	d_( d )
{
}


AxisType AxisEvent::axisType() const
{
	return d_->axisType;
}


qreal AxisEvent::value() const
{
	return d_->value;
}


qreal AxisEvent::oldValue() const
{
	return d_->oldValue;
}




Device::Device( const int id, Manager * const manager ) :
	d_( new DevicePrivate( this, id, manager ) )
{
	d_->buttonInfos_.resize( Button_TotalButtons );
	d_->axisInfos_.resize( Axis_TotalAxes );
}


Device::~Device()
{
#ifdef Q_OS_LINUX
	d_->_abortThread();

	if ( d_->fileWatcher_ )
		d_->_removeFileWatcher();

	if ( d_->fileHandle_ != 0 )
	{
		::close( d_->fileHandle_ );
		d_->fileHandle_ = 0;
	}
#endif

	delete d_;
}


void Device::setAxisMapping( const AxisType axisType, const qreal min, const qreal max )
{
	FLAT_ASSERT(axisType > 0 && axisType < Axis_TotalAxes);
	AxisInfo & axisInfo = d_->axisInfos_[ axisType ];
	axisInfo.minMapping = min;
	axisInfo.maxMapping = max;
	axisInfo.mappingLeft = qMin( axisInfo.minMapping, axisInfo.maxMapping );
	axisInfo.mappingRight = qMax( axisInfo.minMapping, axisInfo.maxMapping );
	axisInfo.mappingRange = axisInfo.maxMapping - axisInfo.minMapping;
}


qreal Device::axisDeadZone( const AxisType axisType ) const
{
	FLAT_ASSERT(axisType > 0 && axisType < Axis_TotalAxes);
	return d_->axisInfos_.at( axisType ).deadZone;
}


void Device::setAxisDeadZone( const AxisType axisType, const qreal value )
{
	FLAT_ASSERT(axisType > 0 && axisType < Axis_TotalAxes);
	d_->axisInfos_[ axisType ].deadZone = qBound<qreal>( 0.0, value, 1.0 );
}
#endif




#if 0
Manager::D::D(Manager * const manager ) :
	manager_( manager )
{
	activeDeviceCount_ = 0;
}


ManagerPrivate::~ManagerPrivate()
{
	for ( Tools::IdGeneratorIterator it( deviceIdGenerator_ ); it.hasNext(); )
		delete deviceForId_.at( it.next() );

	deviceForId_.clear();
	deviceIdGenerator_ = Tools::IdGenerator();
	deviceIds_.clear();

#ifdef Q_OS_LINUX
	deviceForFilePath_.clear();
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_.clear();
#endif
}


void ManagerPrivate::deviceActivated( const bool on )
{
	if ( !on )
	{
		FLAT_ASSERT(activeDeviceCount_ > 0);
		activeDeviceCount_--;

#ifdef Q_OS_WIN
		if ( activeDeviceCount_ == 0 )
			activeDevicesProcessTimer_.stop();
#endif
	}
	else
	{
		activeDeviceCount_++;

#ifdef Q_OS_WIN
		if ( activeDeviceCount_ == 1 )
			activeDevicesProcessTimer_.start( 1, this );
#endif
	}
}


void ManagerPrivate::timerEvent( QTimerEvent * const e )
{
#ifdef Q_OS_WIN
	if ( e->timerId() == activeDevicesProcessTimer_.timerId() )
	{
		_processActiveDevicesWin();
		return;
	}
#endif

	QObject::timerEvent( e );
}


void ManagerPrivate::_deviceAttached( Device * const device )
{
	if ( deviceForId_.size() <= device->deviceId() )
		deviceForId_.resize( device->deviceId() + 1 );
	deviceForId_[ device->deviceId() ] = device;

#ifdef Q_OS_LINUX
	deviceForFilePath_[ device->d_->filePath_ ] = device;
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_[ device->d_->joystickId_ ] = device;
#endif

	deviceIds_ << device->deviceId();

	// notify
	emit manager_->deviceAttached( device->deviceId() );
}


void ManagerPrivate::_deviceDetached( Device * const device )
{
	const int deviceId = device->deviceId();

	// notify
	emit manager_->deviceDetached( deviceId );

	device->setActive( false );

#ifdef Q_OS_LINUX
	deviceForFilePath_.remove( device->d_->filePath_ );
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_.remove( device->d_->joystickId_ );
#endif
	deviceIds_.removeOne( deviceId );
	deviceForId_[ deviceId ] = 0;

	deviceIdGenerator_.free( deviceId );

	device->deleteLater();
}


void ManagerPrivate::_buttonChanged( Device * const device, const ButtonType buttonType, const bool isDown )
{
	ButtonInfo & buttonInfo = device->d_->buttonInfos_[ buttonType ];
	if ( !buttonInfo.isValid )
		return;

	if ( buttonInfo.down == Tools::cleanBoolean( isDown ) )
		return;

	buttonInfo.down = Tools::cleanBoolean( isDown );

	ButtonEventPrivate buttonEventPrivate( buttonType, buttonInfo.down );
	ButtonEvent buttonEvent( &buttonEventPrivate );
	QCoreApplication::sendEvent( device, &buttonEvent );
}


void ManagerPrivate::_axisChanged( Device * const device, const AxisType axisType, const int value )
{
	AxisInfo & axisInfo = device->d_->axisInfos_[ axisType ];
	if ( !axisInfo.isValid )
		return;

	if ( axisInfo.value == value )
		return;

	const qreal oldValue = axisInfo.realValue;
	axisInfo.value = value;

	// Map value onto specified min/max range using linear transformation.
	// Standard canonic line formula used:
	//   (maxValue - minValue)/(value - minValue) = (maxMapping - minMapping)/(realValue - minMapping)
	//   thus
	//   realValue = (maxMapping - minMapping)*(value - minValue)/(maxValue - minValue) + minMapping
	//axisInfo.realValue = qBound( -1.0, qreal(value - axisInfo.minValue)/axisInfo.range*2.0 - 1.0, 1.0 );
	axisInfo.realValue = axisInfo.mappingRange*(axisInfo.value - axisInfo.minValue)/axisInfo.range + axisInfo.minMapping;

	// bound mapped value to allowed range
	axisInfo.realValue = qBound( axisInfo.mappingLeft, axisInfo.realValue, axisInfo.mappingRight );

	// check the dead zone
	if ( axisInfo.deadZone != 0 )
	{
		if ( axisInfo.realValue > -axisInfo.deadZone && axisInfo.realValue < axisInfo.deadZone )
			axisInfo.realValue = 0;
	}

	if ( oldValue == axisInfo.realValue )
		return;

	AxisEventPrivate axisEventPrivate( axisType, axisInfo.realValue, oldValue );
	AxisEvent axisEvent( &axisEventPrivate );
	QCoreApplication::sendEvent( device, &axisEvent );
}
#endif




struct Manager::D
{
	struct DeviceInfo
	{
		Manager & manager;
		const Utils::IdGenerator::Id uniqueId;
		const std::string path;

		Input::Fd fd;
		Utils::IdGenerator::Id id;
		std::string name;
		int vendorId;
		int productId;

		Device::Callbacks * callbacks = nullptr;
		std::unique_ptr<Input::Connection> lockConnection{};

		void lock(Device::Callbacks & callbacks);
		void unlock();

		void _read() noexcept;

		Device & toDevice() noexcept { return reinterpret_cast<Device&>(*this); }
		static DeviceInfo & fromDevice(Device & d) noexcept { return reinterpret_cast<DeviceInfo&>(d); }

		const Device & toDevice() const noexcept { return reinterpret_cast<const Device&>(*this); }
		static const DeviceInfo & fromDevice(const Device & d) noexcept { return reinterpret_cast<const DeviceInfo&>(d); }
	};

	D(Manager & manager, Input::EventDispatcher & eventDispatcher,
			Manager::Callbacks & callbacks) noexcept :
		manager(manager),
		eventDispatcher(eventDispatcher),
		callbacks(callbacks)
	{}

	Manager & manager;
	Input::EventDispatcher & eventDispatcher;
	Manager::Callbacks & callbacks;

	Input::Fd inotifyFd;
	std::unique_ptr<Input::Connection> inotifyConnection;

	Input::Fd devInputFd;

	Utils::IdGenerator deviceIdGenerator;
	std::vector<int> deviceUniqueIdForId;

	Utils::IdGenerator deviceUniqueIdGenerator;
	std::unordered_map<std::string, int> deviceUniqueIdForPath;
	std::vector<std::unique_ptr<DeviceInfo>> deviceInfoForUniqueId;

	void _addDevice(const char * path);
	void _scanDevices();
	void _addDeviceInfo(std::unique_ptr<DeviceInfo> deviceInfo);
	void _readInotify() noexcept;
	std::unique_ptr<DeviceInfo> _openDevice(const char * path);
	void _closeDeviceForPath(const char * path) noexcept;
};




void Manager::D::DeviceInfo::lock(Device::Callbacks & callbacks)
{
	FLAT_ASSERT(fd.isValid());
	FLAT_ASSERT(!lockConnection);

	FLAT_ASSERT(!this->callbacks);
	this->callbacks = &callbacks;

	auto c = manager.d_->eventDispatcher.addHandle(Input::Connection::Type::Read,
			fd.fd(), std::bind(&DeviceInfo::_read, this));

	lockConnection = std::move(c);
}


void Manager::D::DeviceInfo::unlock()
{
	FLAT_ASSERT(this->callbacks);
	FLAT_ASSERT(lockConnection);

	lockConnection.reset();
	this->callbacks = nullptr;
}


void Manager::D::DeviceInfo::_read() noexcept
{
	while (true) {
		::input_event ev[64];
		const int res = ::read(fd.fd(), ev, sizeof(ev));
		FLAT_DEBUG << res << sizeof(::input_event);

		if (res == -1) {
			const int err = errno;
			if (err == EAGAIN) break;
			FLAT_ERROR << ::strerror(err);
			break;
		}

		const int count = res / sizeof(::input_event);
		for (int i = 0; i < count; ++i) {
			const auto type = ev[i].type;
			const auto code = ev[i].code;

//			EV_SYN;
//			SYN_REPORT;

			callbacks->button(type, code);

			if (!callbacks) {
				// unlocked inside the callback
				return;
			}
		}
	}
}




struct Manager::SkipDeviceException : public Debug::Error
{
};

struct Manager::IgnoreDeviceException : public Debug::Error
{
	std::shared_ptr<std::unique_ptr<Manager::D::DeviceInfo>> deviceInfo;
};




void Manager::D::_addDevice(const char * const path)
{
	const bool deviceExists = [this, path] {
		const auto it = deviceUniqueIdForPath.find(path);
		return it != deviceUniqueIdForPath.cend();
	}();

	if (deviceExists) {
//		FLAT_INFO << "device already exists:" << path;
		return;
	}

	try {
		auto deviceInfo = _openDevice(path);

		// device is a valid controller
		FLAT_ASSERT(!deviceInfo->id.isNull());
		if (deviceInfo->id->id() >= int(deviceUniqueIdForId.size())) {
			deviceUniqueIdForId.resize(deviceInfo->id->id() + 1);
		}
		deviceUniqueIdForId[deviceInfo->id->id()] = deviceInfo->uniqueId->id();

		Device & device = deviceInfo->toDevice();
		_addDeviceInfo(std::move(deviceInfo));
		callbacks.deviceAdded(device);
	} catch (IgnoreDeviceException & e) {
		// device is valid, but should be ignored
		_addDeviceInfo(std::move(*e.deviceInfo));
	} catch (const SkipDeviceException & e) {
		// error accessing this device, skip
	} catch (const FLAT_DEBUG_EXCEPTION(FlatController) &) {
		FLAT_ERROR << "failed adding device:" << path;
	}
}


void Manager::D::_scanDevices()
{
	struct DirCloser {
		static void close(::DIR * d) noexcept { ::closedir(d); }
	};

	typedef std::unique_ptr<::DIR, decltype(&DirCloser::close)> Dir;

	const Dir dir(::opendir(kDevInputPath), &DirCloser::close);
	if (!dir) return;

	char path[PATH_MAX];
	std::strcpy(path, kDevInputPath);
	constexpr int kDevInputLength = std::extent_v<decltype(kDevInputPath)> - 1;
	path[kDevInputLength] = '/';

	char * const suffix = path + kDevInputLength + 1;

	while (true) {
		::dirent * const de = ::readdir(dir.get());
		if (!de) break;

		// skip . and .. subdirs
		if (de->d_name[0] == '.') {
			if (de->d_name[1] == '\0') continue;
			if (de->d_name[1] == '.' && de->d_name[2] == '\0') continue;
		}

		std::strcpy(suffix, de->d_name);

		_addDevice(path);
	}
}


void Manager::D::_addDeviceInfo(std::unique_ptr<DeviceInfo> deviceInfo)
{
	const int uniqueId = deviceInfo->uniqueId->id();

	deviceUniqueIdForPath[deviceInfo->path] = uniqueId;

	if (uniqueId >= int(deviceInfoForUniqueId.size())) {
		deviceInfoForUniqueId.resize(uniqueId + 1);
	}
	deviceInfoForUniqueId[uniqueId] = std::move(deviceInfo);
}


void Manager::D::_readInotify() noexcept
{
	uint8_t buf[512];
	int res = ::read(inotifyFd.fd(), buf, sizeof(buf));
	if (res < int(sizeof(::inotify_event))) {
		if (errno == EINTR) return;
		FLAT_WARNING << "No event:" << strerror(errno);
		return;
	}

	int pos = 0;
	while (res >= int(sizeof(::inotify_event))) {
		const ::inotify_event & event = *reinterpret_cast<const ::inotify_event*>(buf + pos);

		if (event.len != 0) {
			[&event, this] {
				if (event.wd == devInputFd.fd()) {
					const std::string filePath = std::string(kDevInputPath) + "/" + event.name;
//					FLAT_DEBUG << filePath << event.len << "c:" << bool(event.mask & IN_CREATE)
//							<< "d:" << bool(event.mask & IN_DELETE) << "a:" << bool(event.mask & IN_ATTRIB);

					if (event.mask & (IN_CREATE | IN_ATTRIB)) {
//						FLAT_INFO << "adding device:" << filePath;
						_addDevice(filePath.c_str());
						return;
					}

					if (event.mask & IN_DELETE) {
//						FLAT_INFO << "removing device:" << filePath;
						_closeDeviceForPath(filePath.c_str());
						return;
					}

					return;
				}

				FLAT_WARNING << "unexpected fd:" << event.wd;
			}();
		}

		const int size = sizeof(event) + event.len;
		res -= size;
		pos += size;
	}
}


std::unique_ptr<Manager::D::DeviceInfo> Manager::D::_openDevice(const char * const path)
{
	Input::Fd fd = [path] () -> Input::Fd {
		const int fd = ::open(path, O_RDWR | O_CLOEXEC | O_NONBLOCK);
		if (fd < 0) {
			const int err = errno;
			if (err == EACCES) {
				// ok, device is created, but permissions might not be set yet by udev, wait for
				// the next IN_ATTRIB event from inotify
				throw SkipDeviceException{};
			}
			if (err == EISDIR) {
				// path is a dir, skip
				throw SkipDeviceException{};
			}
			FLAT_FAIL2(SkipDeviceException{}) << "failed opening device:" << path << err << ::strerror(err);
		}
		return Input::Fd(fd);
	}();

	Utils::IdGenerator::Id uniqueId = deviceUniqueIdGenerator.takeUnique();

	const std::string name = [&fd, path] () -> std::string {
		char buffer[80];
		const int res = ::ioctl(fd.fd(), EVIOCGNAME(sizeof(buffer) - 1), &buffer);
		if (res < 1) {
			FLAT_ERROR << "could not get device name for path:" << path << ::strerror(errno);
			return {};
		}
		buffer[sizeof(buffer) - 1] = '\0';
		return buffer;
	}();

#if 0
	// Check to see if the device is on our excluded list
	for (size_t i = 0; i < mExcludedDevices.size(); i++) {
		const std::string& item = mExcludedDevices[i];
		if (identifier.name == item) {
			ALOGI("ignoring event id %s driver %s\n", devicePath, item.c_str());
			close(fd);
			return -1;
		}
	}
#endif

	const auto ignore = [this, &uniqueId, path] () -> IgnoreDeviceException {
		IgnoreDeviceException e;
		auto deviceInfo = std::unique_ptr<DeviceInfo>(new DeviceInfo{manager, std::move(uniqueId),
				path});
		e.deviceInfo = std::make_shared<std::unique_ptr<DeviceInfo>>(std::move(deviceInfo));
		return e;
	};

	const int driverVersion = [&fd, path, &ignore] {
		int ver;
		const int res = ::ioctl(fd.fd(), EVIOCGVERSION, &ver);
		if (res != 0) {
			FLAT_DEBUG << "could not get driver version for path:" << path << ::strerror(errno);
			throw ignore();
		}
		return ver;
	}();

	const ::input_id inputId = [&fd, path, &ignore] () -> ::input_id {
		::input_id id;
		const int res = ::ioctl(fd.fd(), EVIOCGID, &id);
		if (res != 0) {
			FLAT_DEBUG << "could not get device input id for path:" << path << ::strerror(errno);
			throw ignore();
		}
		return id;
	}();

#if 0
	identifier.bus = inputId.bustype;
	identifier.product = inputId.product;
	identifier.vendor = inputId.vendor;
	identifier.version = inputId.version;
#endif

	const std::string location = [&fd] () -> std::string {
		char buffer[80];
		const int res = ::ioctl(fd.fd(), EVIOCGPHYS(sizeof(buffer) - 1), &buffer);
		if (res < 1) {
			// fprintf(stderr, "could not get location for %s, %s\n", devicePath, strerror(errno));
			return {};
		}
		buffer[sizeof(buffer) - 1] = '\0';
		return buffer;
	}();

	const std::string uniqueStringId = [&fd] () -> std::string {
		char buffer[80];
		const int res = ::ioctl(fd.fd(), EVIOCGUNIQ(sizeof(buffer) - 1), &buffer);
		if (res < 1) {
			// fprintf(stderr, "could not get idstring for %s, %s\n", devicePath, strerror(errno));
			return {};
		}
		buffer[sizeof(buffer) - 1] = '\0';
		return buffer;
	}();

#if 0
	// Fill in the descriptor.
	assignDescriptorLocked(identifier);
#endif

	uint8_t ffFeatures[FF_MAX / 8 + 1];
	std::memset(ffFeatures, 0, sizeof(ffFeatures));
	const bool hasff = [&fd, &ffFeatures] () -> bool {
		const int res = ::ioctl(fd.fd(), EVIOCGBIT(EV_FF, sizeof(ffFeatures)), ffFeatures);
		FLAT_DEBUG << "ff res:" << res;
		return res != -1;
	}();
	FLAT_DEBUG << "hasff:" << hasff;

	if (hasff) {
		const auto testff = [&ffFeatures] (int bit) -> bool {
			return (ffFeatures[bit / 8] >> (bit % 8)) & 1;
		};
		FLAT_DEBUG << "has gain:" << testff(FF_GAIN);

		if (testff(FF_GAIN)) {
			::input_event gain;
			std::memset(&gain, 0, sizeof(gain));
			gain.type = EV_FF;
			gain.code = FF_GAIN;
			gain.value = 0x4000; /* [0, 0xFFFF]) */

			FLAT_DEBUG << "Setting master gain to 25%% ... ";
			if (::write(fd.fd(), &gain, sizeof(gain)) != sizeof(gain)) {
				FLAT_DEBUG << "gain error";
			} else {
				FLAT_DEBUG << "gain ok";
			}

			::ff_effect effect;
			std::memset(&effect, 0, sizeof(effect));
			effect.type = FF_PERIODIC;
			effect.id = -1;
			effect.u.periodic.waveform = FF_SINE;
			effect.u.periodic.period = 100;	/* 0.1 second */
			effect.u.periodic.magnitude = 0x7fff;	/* 0.5 * Maximum magnitude */
			effect.u.periodic.offset = 0;
			effect.u.periodic.phase = 0;
			effect.direction = 0x4000;	/* Along X axis */
			effect.u.periodic.envelope.attack_length = 1000;
			effect.u.periodic.envelope.attack_level = 0x7fff;
			effect.u.periodic.envelope.fade_length = 1000;
			effect.u.periodic.envelope.fade_level = 0x7fff;
			effect.trigger.button = 0;
			effect.trigger.interval = 0;
			effect.replay.length = 2000;  /* 2 seconds */
			effect.replay.delay = 1000;

			FLAT_DEBUG << "Uploading effect #0 (Periodic sinusoidal) ... ";
			if (::ioctl(fd.fd(), EVIOCSFF, &effect) == -1) {
				FLAT_DEBUG << "error";
			} else {
				FLAT_DEBUG << "OK" << effect.id;
			}

			::input_event play;
			std::memset(&play, 0, sizeof(play));
			play.type = EV_FF;
			play.code = effect.id;
			play.value = 1;

			if (::write(fd.fd(), &play, sizeof(play)) == -1) {
				FLAT_DEBUG << "error playing";
			} else {
				FLAT_DEBUG << "Now Playing ...";
			}
		}
	}

#if 0
	// Allocate device.  (The device object takes ownership of the fd at this point.)
	int32_t deviceId = mNextDeviceId++;
	Device* device = new Device(fd, deviceId, devicePath, identifier);

	ALOGV("add device %d: %s\n", deviceId, devicePath);
	ALOGV("  bus:        %04x\n"
		  "  vendor      %04x\n"
		  "  product     %04x\n"
		  "  version     %04x\n",
		  identifier.bus, identifier.vendor, identifier.product, identifier.version);
	ALOGV("  name:       \"%s\"\n", identifier.name.c_str());
	ALOGV("  location:   \"%s\"\n", identifier.location.c_str());
	ALOGV("  unique id:  \"%s\"\n", identifier.uniqueId.c_str());
	ALOGV("  descriptor: \"%s\"\n", identifier.descriptor.c_str());
	ALOGV("  driver:     v%d.%d.%d\n", driverVersion >> 16, (driverVersion >> 8) & 0xff,
		  driverVersion & 0xff);

	// Load the configuration file for the device.
	loadConfigurationLocked(device);

	// Figure out the kinds of events the device reports.
	ioctl(fd, EVIOCGBIT(EV_KEY, sizeof(device->keyBitmask)), device->keyBitmask);
	ioctl(fd, EVIOCGBIT(EV_ABS, sizeof(device->absBitmask)), device->absBitmask);
	ioctl(fd, EVIOCGBIT(EV_REL, sizeof(device->relBitmask)), device->relBitmask);
	ioctl(fd, EVIOCGBIT(EV_SW, sizeof(device->swBitmask)), device->swBitmask);
	ioctl(fd, EVIOCGBIT(EV_LED, sizeof(device->ledBitmask)), device->ledBitmask);
	ioctl(fd, EVIOCGBIT(EV_FF, sizeof(device->ffBitmask)), device->ffBitmask);
	ioctl(fd, EVIOCGPROP(sizeof(device->propBitmask)), device->propBitmask);

	// See if this is a keyboard.  Ignore everything in the button range except for
	// joystick and gamepad buttons which are handled like keyboards for the most part.
	bool haveKeyboardKeys =
			containsNonZeroByte(device->keyBitmask, 0, sizeof_bit_array(BTN_MISC)) ||
			containsNonZeroByte(device->keyBitmask, sizeof_bit_array(KEY_OK),
								sizeof_bit_array(KEY_MAX + 1));
	bool haveGamepadButtons = containsNonZeroByte(device->keyBitmask, sizeof_bit_array(BTN_MISC),
												  sizeof_bit_array(BTN_MOUSE)) ||
			containsNonZeroByte(device->keyBitmask, sizeof_bit_array(BTN_JOYSTICK),
								sizeof_bit_array(BTN_DIGI));
	if (haveKeyboardKeys || haveGamepadButtons) {
		device->classes |= INPUT_DEVICE_CLASS_KEYBOARD;
	}

	// See if this is a cursor device such as a trackball or mouse.
	if (test_bit(BTN_MOUSE, device->keyBitmask) && test_bit(REL_X, device->relBitmask) &&
		test_bit(REL_Y, device->relBitmask)) {
		device->classes |= INPUT_DEVICE_CLASS_CURSOR;
	}

	// See if this is a rotary encoder type device.
	String8 deviceType = String8();
	if (device->configuration &&
		device->configuration->tryGetProperty(String8("device.type"), deviceType)) {
		if (!deviceType.compare(String8("rotaryEncoder"))) {
			device->classes |= INPUT_DEVICE_CLASS_ROTARY_ENCODER;
		}
	}

	// See if this is a touch pad.
	// Is this a new modern multi-touch driver?
	if (test_bit(ABS_MT_POSITION_X, device->absBitmask) &&
		test_bit(ABS_MT_POSITION_Y, device->absBitmask)) {
		// Some joysticks such as the PS3 controller report axes that conflict
		// with the ABS_MT range.  Try to confirm that the device really is
		// a touch screen.
		if (test_bit(BTN_TOUCH, device->keyBitmask) || !haveGamepadButtons) {
			device->classes |= INPUT_DEVICE_CLASS_TOUCH | INPUT_DEVICE_CLASS_TOUCH_MT;
		}
		// Is this an old style single-touch driver?
	} else if (test_bit(BTN_TOUCH, device->keyBitmask) && test_bit(ABS_X, device->absBitmask) &&
			   test_bit(ABS_Y, device->absBitmask)) {
		device->classes |= INPUT_DEVICE_CLASS_TOUCH;
		// Is this a BT stylus?
	} else if ((test_bit(ABS_PRESSURE, device->absBitmask) ||
				test_bit(BTN_TOUCH, device->keyBitmask)) &&
			   !test_bit(ABS_X, device->absBitmask) && !test_bit(ABS_Y, device->absBitmask)) {
		device->classes |= INPUT_DEVICE_CLASS_EXTERNAL_STYLUS;
		// Keyboard will try to claim some of the buttons but we really want to reserve those so we
		// can fuse it with the touch screen data, so just take them back. Note this means an
		// external stylus cannot also be a keyboard device.
		device->classes &= ~INPUT_DEVICE_CLASS_KEYBOARD;
	}

	// See if this device is a joystick.
	// Assumes that joysticks always have gamepad buttons in order to distinguish them
	// from other devices such as accelerometers that also have absolute axes.
	if (haveGamepadButtons) {
		uint32_t assumedClasses = device->classes | INPUT_DEVICE_CLASS_JOYSTICK;
		for (int i = 0; i <= ABS_MAX; i++) {
			if (test_bit(i, device->absBitmask) &&
				(getAbsAxisUsage(i, assumedClasses) & INPUT_DEVICE_CLASS_JOYSTICK)) {
				device->classes = assumedClasses;
				break;
			}
		}
	}

	// Check whether this device has switches.
	for (int i = 0; i <= SW_MAX; i++) {
		if (test_bit(i, device->swBitmask)) {
			device->classes |= INPUT_DEVICE_CLASS_SWITCH;
			break;
		}
	}

	// Check whether this device supports the vibrator.
	if (test_bit(FF_RUMBLE, device->ffBitmask)) {
		device->classes |= INPUT_DEVICE_CLASS_VIBRATOR;
	}

	// Configure virtual keys.
	if ((device->classes & INPUT_DEVICE_CLASS_TOUCH)) {
		// Load the virtual keys for the touch screen, if any.
		// We do this now so that we can make sure to load the keymap if necessary.
		bool success = loadVirtualKeyMapLocked(device);
		if (success) {
			device->classes |= INPUT_DEVICE_CLASS_KEYBOARD;
		}
	}

	// Load the key map.
	// We need to do this for joysticks too because the key layout may specify axes.
	status_t keyMapStatus = NAME_NOT_FOUND;
	if (device->classes & (INPUT_DEVICE_CLASS_KEYBOARD | INPUT_DEVICE_CLASS_JOYSTICK)) {
		// Load the keymap for the device.
		keyMapStatus = loadKeyMapLocked(device);
	}

	// Configure the keyboard, gamepad or virtual keyboard.
	if (device->classes & INPUT_DEVICE_CLASS_KEYBOARD) {
		// Register the keyboard as a built-in keyboard if it is eligible.
		if (!keyMapStatus && mBuiltInKeyboardId == NO_BUILT_IN_KEYBOARD &&
			isEligibleBuiltInKeyboard(device->identifier, device->configuration, &device->keyMap)) {
			mBuiltInKeyboardId = device->id;
		}

		// 'Q' key support = cheap test of whether this is an alpha-capable kbd
		if (hasKeycodeLocked(device, AKEYCODE_Q)) {
			device->classes |= INPUT_DEVICE_CLASS_ALPHAKEY;
		}

		// See if this device has a DPAD.
		if (hasKeycodeLocked(device, AKEYCODE_DPAD_UP) &&
			hasKeycodeLocked(device, AKEYCODE_DPAD_DOWN) &&
			hasKeycodeLocked(device, AKEYCODE_DPAD_LEFT) &&
			hasKeycodeLocked(device, AKEYCODE_DPAD_RIGHT) &&
			hasKeycodeLocked(device, AKEYCODE_DPAD_CENTER)) {
			device->classes |= INPUT_DEVICE_CLASS_DPAD;
		}

		// See if this device has a gamepad.
		for (size_t i = 0; i < sizeof(GAMEPAD_KEYCODES) / sizeof(GAMEPAD_KEYCODES[0]); i++) {
			if (hasKeycodeLocked(device, GAMEPAD_KEYCODES[i])) {
				device->classes |= INPUT_DEVICE_CLASS_GAMEPAD;
				break;
			}
		}
	}

	// If the device isn't recognized as something we handle, don't monitor it.
	if (device->classes == 0) {
		ALOGV("Dropping device: id=%d, path='%s', name='%s'", deviceId, devicePath,
			  device->identifier.name.c_str());
		delete device;
		return -1;
	}

	// Determine whether the device has a mic.
	if (deviceHasMicLocked(device)) {
		device->classes |= INPUT_DEVICE_CLASS_MIC;
	}

	// Determine whether the device is external or internal.
	if (isExternalDeviceLocked(device)) {
		device->classes |= INPUT_DEVICE_CLASS_EXTERNAL;
	}

	if (device->classes & (INPUT_DEVICE_CLASS_JOYSTICK | INPUT_DEVICE_CLASS_DPAD) &&
		device->classes & INPUT_DEVICE_CLASS_GAMEPAD) {
		device->controllerNumber = getNextControllerNumberLocked(device);
		setLedForControllerLocked(device);
	}

	// Find a matching video device by comparing device names
	// This should be done before registerDeviceForEpollLocked, so that both fds are added to epoll
	for (std::unique_ptr<TouchVideoDevice>& videoDevice : mUnattachedVideoDevices) {
		if (device->identifier.name == videoDevice->getName()) {
			device->videoDevice = std::move(videoDevice);
			break;
		}
	}
	mUnattachedVideoDevices
			.erase(std::remove_if(mUnattachedVideoDevices.begin(), mUnattachedVideoDevices.end(),
								  [](const std::unique_ptr<TouchVideoDevice>& videoDevice) {
									  return videoDevice == nullptr;
								  }),
				   mUnattachedVideoDevices.end());

	if (registerDeviceForEpollLocked(device) != OK) {
		delete device;
		return -1;
	}

	configureFd(device);

	ALOGI("New device: id=%d, fd=%d, path='%s', name='%s', classes=0x%x, "
		  "configuration='%s', keyLayout='%s', keyCharacterMap='%s', builtinKeyboard=%s, ",
		  deviceId, fd, devicePath, device->identifier.name.c_str(), device->classes,
		  device->configurationFile.c_str(), device->keyMap.keyLayoutFile.c_str(),
		  device->keyMap.keyCharacterMapFile.c_str(), toString(mBuiltInKeyboardId == deviceId));

	addDeviceLocked(device);
	return OK;
#endif

	FLAT_DEBUG << path << uniqueId->id() << name;
	FLAT_DEBUG << location;
	FLAT_DEBUG << uniqueStringId;
	FLAT_DEBUG << driverVersion;
	FLAT_DEBUG << inputId.vendor << inputId.bustype << inputId.product << inputId.version;

	return std::unique_ptr<DeviceInfo>(new DeviceInfo{manager, std::move(uniqueId), path,
			std::move(fd), deviceIdGenerator.takeUnique(), std::move(name),
			inputId.vendor, inputId.product});
}


void Manager::D::_closeDeviceForPath(const char * const path) noexcept
{
	const auto it = deviceUniqueIdForPath.find(path);
	if (it == deviceUniqueIdForPath.cend()) return;

	std::unique_ptr<DeviceInfo> & deviceInfo = deviceInfoForUniqueId[it->second];

	if (!deviceInfo->id.isNull()) {
		callbacks.deviceRemoved(deviceInfo->toDevice());
		FLAT_ASSERT(!deviceInfo->callbacks);
	}

	deviceInfo.reset();

	deviceUniqueIdForPath.erase(it);
}


#if 0
void EventHub::configureFd(Device* device) {
    // Set fd parameters with ioctl, such as key repeat, suspend block, and clock type
    if (device->classes & INPUT_DEVICE_CLASS_KEYBOARD) {
        // Disable kernel key repeat since we handle it ourselves
        unsigned int repeatRate[] = {0, 0};
        if (ioctl(device->fd, EVIOCSREP, repeatRate)) {
            ALOGW("Unable to disable kernel key repeat for %s: %s", device->path.c_str(),
                  strerror(errno));
        }
    }

    std::string wakeMechanism = "EPOLLWAKEUP";
    if (!mUsingEpollWakeup) {
#ifndef EVIOCSSUSPENDBLOCK
        // uapi headers don't include EVIOCSSUSPENDBLOCK, and future kernels
        // will use an epoll flag instead, so as long as we want to support
        // this feature, we need to be prepared to define the ioctl ourselves.
#define EVIOCSSUSPENDBLOCK _IOW('E', 0x91, int)
#endif
        if (ioctl(device->fd, EVIOCSSUSPENDBLOCK, 1)) {
            wakeMechanism = "<none>";
        } else {
            wakeMechanism = "EVIOCSSUSPENDBLOCK";
        }
    }
    // Tell the kernel that we want to use the monotonic clock for reporting timestamps
    // associated with input events.  This is important because the input system
    // uses the timestamps extensively and assumes they were recorded using the monotonic
    // clock.
    int clockId = CLOCK_MONOTONIC;
    bool usingClockIoctl = !ioctl(device->fd, EVIOCSCLOCKID, &clockId);
    ALOGI("wakeMechanism=%s, usingClockIoctl=%s", wakeMechanism.c_str(), toString(usingClockIoctl));
}
#endif




int Device::id() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	FLAT_ASSERT(!deviceInfo.id.isNull());
	return deviceInfo.id->id();
}


Flat::StringRef Device::path() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	return Flat::StringRef(deviceInfo.path);
}


Flat::StringRef Device::name() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	return Flat::StringRef(deviceInfo.name);
}


int Device::vendorId() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	return deviceInfo.vendorId;
}


int Device::productId() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	return deviceInfo.productId;
}


bool Device::isLocked() const noexcept
{
	const auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	return deviceInfo.callbacks != nullptr;
}


Device::Lock Device::lock(Callbacks & callbacks) noexcept
{
	auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*this);
	deviceInfo.lock(callbacks);
	return Lock(*this);
}




Device::Lock::Lock() :
	device_(nullptr)
{
}


Device::Lock::~Lock()
{
	reset();
}


Device::Lock::Lock(Lock && other) noexcept :
	device_(other.device_)
{
	other.device_ = nullptr;
}


Device::Lock & Device::Lock::operator=(Lock && other) noexcept
{
	reset();
	device_ = other.device_;
	other.device_ = nullptr;
	return *this;
}


Device::Lock::Lock(Device & device) noexcept :
	device_(&device)
{
}


void Device::Lock::reset()
{
	if (device_) {
		auto & deviceInfo = Manager::D::DeviceInfo::fromDevice(*device_);
		deviceInfo.unlock();
		device_ = nullptr;
	}
}




Manager::Manager(Input::EventDispatcher & eventDispatcher, Callbacks & callbacks) :
	d_(new D(*this, eventDispatcher, callbacks))
{
	Input::Fd inotifyFd(::inotify_init());
	Input::Fd devInputFd(::inotify_add_watch(inotifyFd.fd(), kDevInputPath,
			IN_DELETE | IN_CREATE | IN_ATTRIB));

	auto inotifyConnection = eventDispatcher.addHandle(Input::Connection::Type::Read,
			inotifyFd.fd(), std::bind(&D::_readInotify, d_.get()));

	d_->inotifyFd = std::move(inotifyFd);
	d_->inotifyConnection = std::move(inotifyConnection);
	d_->devInputFd = std::move(devInputFd);

	d_->_scanDevices();
}


Manager::~Manager()
{
}




}}




namespace Flat {
namespace Debug {

template <>
inline void Log::append<Flat::Controller::Button>(const Flat::Controller::Button & button) noexcept
{
	State state(*this);

	maybeSpace();
	*this << NoSpace();

#define FLAT_CONTROLLER_LOG_BUTTON(x) \
	case Flat::Controller::Button::x: appendString(#x); return

	switch (button) {
	FLAT_CONTROLLER_LOG_BUTTON(Null);
	FLAT_CONTROLLER_LOG_BUTTON(A);
	FLAT_CONTROLLER_LOG_BUTTON(B);
	FLAT_CONTROLLER_LOG_BUTTON(C);
	FLAT_CONTROLLER_LOG_BUTTON(X);
	FLAT_CONTROLLER_LOG_BUTTON(Y);
	FLAT_CONTROLLER_LOG_BUTTON(Z);
	FLAT_CONTROLLER_LOG_BUTTON(TL);
	FLAT_CONTROLLER_LOG_BUTTON(TR);
	FLAT_CONTROLLER_LOG_BUTTON(TL2);
	FLAT_CONTROLLER_LOG_BUTTON(TR2);
	FLAT_CONTROLLER_LOG_BUTTON(Select);
	FLAT_CONTROLLER_LOG_BUTTON(Start);
	FLAT_CONTROLLER_LOG_BUTTON(Mode);
	FLAT_CONTROLLER_LOG_BUTTON(ThumbL);
	FLAT_CONTROLLER_LOG_BUTTON(ThumbR);

	case Flat::Controller::Button::ButtonCount:
		break;
	}

#undef FLAT_CONTROLLER_LOG_BUTTON

	*this << "(Invalid button: " << int(button) << ")";
}

template <>
inline void Log::append<Flat::Controller::Axis>(const Flat::Controller::Axis & axis) noexcept
{
	State state(*this);

	maybeSpace();
	*this << NoSpace();

#define FLAT_CONTROLLER_LOG_AXIS(x) \
	case Flat::Controller::Axis::x: appendString(#x); return;

	switch (axis) {
	FLAT_CONTROLLER_LOG_AXIS(Null)
	FLAT_CONTROLLER_LOG_AXIS(X)
	FLAT_CONTROLLER_LOG_AXIS(Y)
	FLAT_CONTROLLER_LOG_AXIS(Z)
	FLAT_CONTROLLER_LOG_AXIS(R)
	FLAT_CONTROLLER_LOG_AXIS(U)
	FLAT_CONTROLLER_LOG_AXIS(V)
	FLAT_CONTROLLER_LOG_AXIS(RX)
	FLAT_CONTROLLER_LOG_AXIS(RY)
	FLAT_CONTROLLER_LOG_AXIS(RZ)
	FLAT_CONTROLLER_LOG_AXIS(PovX)
	FLAT_CONTROLLER_LOG_AXIS(PovY)

	case Flat::Controller::Axis::AxisCount:
		break;
	}

#undef FLAT_CONTROLLER_LOG_AXIS

	*this << "(Invalid axis type: " << int(axis) << ")";
}

}}
