
#pragma once

#include <memory>

#include <Flat/Debug/Log.hpp>
#include <Flat/Input/Handle.hpp>




namespace Flat {
namespace Input {
	class EventDispatcher;
}}




namespace Flat {
namespace Controller {




class ButtonEventPrivate;
class AxisEventPrivate;
class DevicePrivate;
class ManagerPrivate;
class Manager;




enum class Button
{
	Null = 0,

	A,
	B,
	C,
	X,
	Y,
	Z,
	TL,
	TR,
	TL2,
	TR2,
	Select,
	Start,
	Mode,
	ThumbL,
	ThumbR,

	ButtonCount
};




enum class Axis
{
	Null = 0,

	X,
	Y,
	Z,
	R,
	U,
	V,
	RX,
	RY,
	RZ,
	PovX,
	PovY,

	AxisCount
};




#if 0
enum EventType
{
	Event_Button = QEvent::User + 1,
	Event_Axis
};




class FLAT_CONTROLLER_EXPORT ButtonEvent : public QEvent
{
public:
	ButtonType buttonType() const;
	bool isDown() const;

private:
	ButtonEvent( ButtonEventPrivate * d );
	ButtonEventPrivate * const d_;

	friend class ManagerPrivate;
};




class FLAT_CONTROLLER_EXPORT AxisEvent
{
public:
	AxisType axisType() const;
	qreal value() const;
	qreal oldValue() const;

private:
	AxisEvent( AxisEventPrivate * d );
	AxisEventPrivate * const d_;

	friend class ManagerPrivate;
};
#endif




class FLAT_CONTROLLER_EXPORT Device
{
public:
	struct Callbacks
	{
		virtual ~Callbacks() = default;
		virtual void button(int type, int code) = 0;
	};

	class Lock
	{
	public:
		Lock();
		~Lock();

		Lock(Lock && other) noexcept;
		Lock & operator=(Lock && other) noexcept;

		operator bool() const noexcept { return device_ != nullptr; }
		Device & device() noexcept { return *device_; }

		void reset();

		Lock(const Lock &) = delete;
		void operator=(const Lock &) = delete;

	private:
		Lock(Device & device) noexcept;

		Device * device_;

		friend class Device;
	};

	Device(const Device &) = delete;
	void operator=(const Device &) = delete;

	int id() const noexcept;
	Flat::StringRef path() const noexcept;
	Flat::StringRef name() const noexcept;
	int vendorId() const noexcept;
	int productId() const noexcept;

	bool isLocked() const noexcept;
	Lock lock(Callbacks & callbacks) noexcept;

#if 0
	void setAxisMapping( AxisType axisType, qreal min, qreal max );

	qreal axisDeadZone( AxisType axisType ) const;
	void setAxisDeadZone( AxisType axisType, qreal value );
#endif
};




class FLAT_CONTROLLER_EXPORT Manager
{
public:
	struct Callbacks
	{
		virtual ~Callbacks() = default;
		virtual void deviceAdded(Device & device) = 0;
		virtual void deviceRemoved(Device & device) = 0;
	};

	Manager(Input::EventDispatcher & eventDispatcher, Callbacks & calbacks);
	~Manager();

private:
	struct D;
	struct SkipDeviceException;
	struct IgnoreDeviceException;

	const std::unique_ptr<D> d_;

	friend class Device;
};




}}




namespace Flat {
namespace Debug {

template <>
void Log::append<Flat::Controller::Button>(const Flat::Controller::Button & button) noexcept;

template <>
inline void Log::append<Flat::Controller::Axis>(const Flat::Controller::Axis & axis) noexcept;

}}
