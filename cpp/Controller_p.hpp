
#pragma once

#ifdef __WIN32
#include <windows.h>
#endif

#include <Flat/Utils/IdGenerator.hpp>

#include "Controller.hpp"




namespace Flat {
namespace Controller {




#if 0
#ifdef Q_OS_LINUX
struct QueueEvent
{
	ButtonType buttonType;
	AxisType axisType;
	int value;
};
#endif

#ifdef Q_OS_WIN
typedef qint64 JoystickId;
#endif




class ManagerPrivate;




enum DevicePrivateEventType
{
	DevicePrivateEvent_Error = QEvent::User + 1,
	DevicePrivateEvent_Update
};




class DevicePrivateErrorEvent : public QEvent
{
public:
	DevicePrivateErrorEvent() :
		QEvent( static_cast<QEvent::Type>( DevicePrivateEvent_Error ) )
	{}
};




class DevicePrivateUpdateEvent : public QEvent
{
public:
	DevicePrivateUpdateEvent() :
		QEvent( static_cast<QEvent::Type>( DevicePrivateEvent_Update ) )
	{}
};




class ButtonEventPrivate
{
public:
	ButtonEventPrivate( ButtonType _buttonType, bool _isDown ) :
		buttonType( _buttonType ), isDown( _isDown )
	{}

	ButtonType buttonType;
	bool isDown;
};




class AxisEventPrivate
{
public:
	AxisEventPrivate( AxisType _axisType, qreal _value, qreal _oldValue ) :
		axisType( _axisType ), value( _value ), oldValue( _oldValue )
	{}

	AxisType axisType;
	qreal value;
	qreal oldValue;
};




class ButtonInfo
{
public:
	ButtonInfo() :
		isValid( false ), down( false )
	{}

	bool isValid;

	bool down;
};




class AxisInfo
{
public:
	AxisInfo()
	{
		isValid = false;

		minValue = 0;
		maxValue = 0;
		range = maxValue - minValue;

		deadZone = 0;
		minMapping = -1.0;
		maxMapping = 1.0;

		mappingLeft = qMin( minMapping, maxMapping );
		mappingRight = qMax( minMapping, maxMapping );
		mappingRange = maxMapping - minMapping;

		value = 0;
		realValue = 0;
	}

	bool isValid;
	int minValue;
	int maxValue;
	int range;

	qreal deadZone;
	qreal minMapping;
	qreal maxMapping;

	qreal mappingLeft;
	qreal mappingRight;
	qreal mappingRange;

	int value;
	qreal realValue;
};




#ifdef Q_OS_LINUX
class DeviceThread : public QThread
{
protected:
	void run();

public:
	Device * device;
};
#endif




class DevicePrivate : public QObject
{
	Q_OBJECT

public:
	DevicePrivate( Device * device, int id, Manager * manager );

#ifdef Q_OS_LINUX
	void threadBody();
#endif

#ifdef Q_OS_LINUX
protected:
	bool event( QEvent * e );
#endif

private:
#ifdef Q_OS_LINUX
	void _fileChanged();
#endif

	void _createFileWatcher();
	void _removeFileWatcher();

#ifdef Q_OS_LINUX
	void _abortThread();
#endif

private:
	Device * const device_;
	const int id_;
	Manager * const manager_;

	int vendorId_;
	int productId_;

	QString name_;

	QVector<ButtonInfo> buttonInfos_;
	QVector<AxisInfo> axisInfos_;

	bool isActive_;

#ifdef Q_OS_LINUX
	QFileSystemWatcher * fileWatcher_;
	QString filePath_;
	int fileHandle_;
	bool isFileHandleBroken_;
	DeviceThread thread_;
	QMutex mutex_;
	QWaitCondition waiter_;
	bool isAborted_;

	QMutex eventQueueMutex_;
	QList<QueueEvent> eventQueue_;
#endif

#ifdef Q_OS_WIN
	JoystickId joystickId_;
	JOYINFOEX joyinfo_;
#endif

	friend class Device;
	friend class ManagerPrivate;
};




class ManagerPrivate
{
	Q_OBJECT

public:
	ManagerPrivate( Manager * manager );
	~ManagerPrivate();

	void updateDevices();

	// called from Device
	void deviceActivated( bool on );

protected:
	void timerEvent( QTimerEvent * e );

private:
	void _deviceAttached( Device * device );
	void _deviceDetached( Device * device );
	void _buttonChanged( Device * device, ButtonType buttonType, bool isDown );
	void _axisChanged( Device * device, AxisType axisType, int value );

#ifdef Q_OS_WIN
	void _povChangedWin( Device * device, DWORD pov );
	void _buttonsChangedWin( Device * device, DWORD buttons );
	void _processActiveDevicesWin();
#endif

private:
	Manager * const manager_;

	Tools::IdGenerator deviceIdGenerator_;
	QVector<Device*> deviceForId_;
	QList<int> deviceIds_;

#ifdef Q_OS_LINUX
	QStringList skipDeviceFilePaths_;
	QMap<QString, Device*> deviceForFilePath_;
#endif

#ifdef Q_OS_WIN
	QMap<JoystickId, Device*> deviceForJoystickId_;
	QBasicTimer activeDevicesProcessTimer_;
#endif

	int activeDeviceCount_;

	friend class Device;
	friend class DevicePrivate;
	friend class Manager;
};
#endif




}}
