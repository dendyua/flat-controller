
#include "Controller.hpp"
#include "Controller_p.hpp"

#include <linux/input.h>
#include <sys/poll.h>
#include <errno.h>
#include <unistd.h>




namespace Flat {
namespace Controller {




#if 0
static ButtonType buttonTypeForKey( const int key )
{
	switch ( key )
	{
	case BTN_A:      return Button_A;
	case BTN_B:      return Button_B;
	case BTN_C:      return Button_C;
	case BTN_X:      return Button_X;
	case BTN_Y:      return Button_Y;
	case BTN_Z:      return Button_Z;
	case BTN_TL:     return Button_TL;
	case BTN_TR:     return Button_TR;
	case BTN_TL2:    return Button_TL2;
	case BTN_TR2:    return Button_TR2;
	case BTN_SELECT: return Button_Select;
	case BTN_START:  return Button_Start;
	case BTN_MODE:   return Button_Mode;
	case BTN_THUMBL: return Button_ThumbL;
	case BTN_THUMBR: return Button_ThumbR;
	}

	return Button_Null;
}


static AxisType axisTypeForAbs( const int abs )
{
	switch ( abs )
	{
	case ABS_X:     return Axis_X;
	case ABS_Y:     return Axis_Y;
	case ABS_Z:     return Axis_Z;
	case ABS_RX:    return Axis_RX;
	case ABS_RY:    return Axis_RY;
	case ABS_RZ:    return Axis_RZ;
	case ABS_HAT0X: return Axis_PovX;
	case ABS_HAT0Y: return Axis_PovY;
	}

	return Axis_Null;
}




void DeviceThread::run()
{
	device->d_->threadBody();
}




void DevicePrivate::threadBody()
{
	while ( true )
	{
		{
			::pollfd p;
			p.fd = fileHandle_;
			p.events = POLLIN;
			p.revents = 0;

			const int status = ::poll( &p, 1, 10 );
			if ( status == -1 || isAborted_ )
				GRIM_DEBUG << "status =" << status << "aborted =" << isAborted_;

			if ( isAborted_ )
				return;

			if ( status < 0 )
			{
				isFileHandleBroken_ = true;
				QCoreApplication::postEvent( this, new DevicePrivateErrorEvent );
				return;
			}

			if ( status == 0 )
				continue;
		}

		::input_event event;
		const bool done = ::read( fileHandle_, &event, sizeof(event) ) == sizeof(event);

		if ( !done || isAborted_ )
			GRIM_DEBUG << "done =" << done << "aborted =" << isAborted_;

		if ( isAborted_ )
		{
			// aborted manually, do nothing
			return;
		}

		if ( !done )
		{
			isFileHandleBroken_ = true;
			QCoreApplication::postEvent( this, new DevicePrivateErrorEvent );
			return;
		}

		switch ( event.type )
		{
		case EV_KEY:
		{
			const ButtonType buttonType = buttonTypeForKey( event.code );
			if ( buttonType == Button_Null )
				continue;

			const ButtonInfo & buttonInfo = buttonInfos_.at( buttonType );
			if ( !buttonInfo.isValid )
				continue;

			QueueEvent queueEvent;
			queueEvent.buttonType = buttonType;
			queueEvent.axisType = Axis_Null;
			queueEvent.value = event.value;

			{
				QMutexLocker locker( &eventQueueMutex_ );
				FLAT_UNUSED(locker)
				eventQueue_ << queueEvent;
			}

			QCoreApplication::postEvent( this, new DevicePrivateUpdateEvent );
		}
			break;

		case EV_ABS:
		{
			const AxisType axisType = axisTypeForAbs( event.code );
			if ( axisType == Axis_Null )
				continue;

			const AxisInfo & axisInfo = axisInfos_.at( axisType );
			if ( !axisInfo.isValid )
				continue;

			QueueEvent queueEvent;
			queueEvent.buttonType = Button_Null;
			queueEvent.axisType = axisType;
			queueEvent.value = event.value;

			{
				QMutexLocker locker( &eventQueueMutex_ );
				FLAT_UNUSED(locker)
				eventQueue_ << queueEvent;
			}

			QCoreApplication::postEvent( this, new DevicePrivateUpdateEvent );
		}
			break;
		}
	}
}


bool DevicePrivate::event( QEvent * const e )
{
	switch ( e->type() )
	{
	case DevicePrivateEvent_Error:
		isFileHandleBroken_ = true;
		manager_->d_->_deviceDetached( device_ );
		return true;

	case DevicePrivateEvent_Update:
	{
		QList<QueueEvent> copy;

		{
			QMutexLocker locker( &eventQueueMutex_ );
			FLAT_UNUSED(locker)
			copy = eventQueue_;
			eventQueue_.clear();
			QCoreApplication::removePostedEvents( this, DevicePrivateEvent_Update );
		}

		QPointer<DevicePrivate> self( this );
		foreach ( const QueueEvent & queueEvent, copy )
		{
			if ( queueEvent.axisType != Axis_Null )
				manager_->d_->_axisChanged( device_, queueEvent.axisType, queueEvent.value );
			else if ( queueEvent.buttonType != Button_Null )
				manager_->d_->_buttonChanged( device_, queueEvent.buttonType, queueEvent.value != 0 );
			if ( !self )
				return true;
		}
	}
		return true;
	}

	return QObject::event( e );
}


void DevicePrivate::_fileChanged()
{
	if ( !QFile::exists( filePath_ ) )
	{
		fileWatcher_->disconnect( this );
		manager_->d_->_deviceDetached( device_ );
	}
}


void DevicePrivate::_createFileWatcher()
{
	FLAT_ASSERT(!fileWatcher_);
	fileWatcher_ = new QFileSystemWatcher( QStringList() << filePath_ );
	connect(fileWatcher_, &QFileSystemWatcher::fileChanged, this, &DevicePrivate::_fileChanged);
}


void DevicePrivate::_removeFileWatcher()
{
	FLAT_ASSERT(fileWatcher_);
	delete fileWatcher_;
	fileWatcher_ = 0;
}


void DevicePrivate::_abortThread()
{
	if ( !thread_.isRunning() )
		return;

	const int oldFileHandle = fileHandle_;
	const int newFileHandle = oldFileHandle != -1 ? ::dup( oldFileHandle ) : -1;

	isAborted_ = true;

	fileHandle_ = -1;
	if ( oldFileHandle != -1 )
		::close( oldFileHandle );

	thread_.wait();

	fileHandle_ = newFileHandle;
}




void ManagerPrivate::updateDevices()
{
	const QDir inputDir = QDir( QLatin1String( "/dev/input" ) );
	const QString filter = QLatin1String( "event*" );
	foreach ( const QString & fileName, inputDir.entryList( QStringList() << filter,
			QDir::Files | QDir::System | QDir::Readable ) )
	{
		const QString filePath = inputDir.filePath( fileName );

		if ( deviceForFilePath_.contains( filePath ) )
			continue;

		if ( skipDeviceFilePaths_.contains( filePath ) )
		{
			GRIM_DEBUG << "Device is skipped";
			continue;
		}

		QFile file( filePath );
		if ( !file.open( QIODevice::ReadOnly ) )
		{
			GRIM_WARNING << "Failed to read file:" << filePath;
			continue;
		}

		int caps[ (EV_CNT - 1) / sizeof(int) * 8 + 1 ];
		int keys[ (KEY_CNT - 1) / sizeof(int) * 8 + 1 ];
		int axes[ (ABS_CNT - 1) / sizeof(int) * 8 + 1 ];
		memset( caps, 0, sizeof(caps) );
		memset( keys, 0, sizeof(keys) );
		memset( axes, 0, sizeof(axes) );

		// check the device capabilities
		if ( ::ioctl( file.handle(), EVIOCGBIT( 0, sizeof(caps) ), caps ) < 0 )
		{
			GRIM_WARNING << "Failed to get capabilities. Error:" << ::strerror( errno );
			continue;
		}

		// device should support at least buttons and axes
		if ( !Tools::testBit( EV_KEY, caps ) || !Tools::testBit( EV_ABS, caps ) )
		{
			GRIM_DEBUG << "Device does not support buttons or axes";
			continue;
		}

		// check the supported buttons and axes
		if ( ::ioctl( file.handle(), EVIOCGBIT( EV_KEY, sizeof(keys) ), keys ) < 0 )
		{
			GRIM_WARNING << "Failed to get supported buttons. Error:" << ::strerror( errno );
			continue;
		}

		if ( ::ioctl( file.handle(), EVIOCGBIT( EV_ABS, sizeof(axes) ), axes ) < 0 )
		{
			GRIM_WARNING << "Failed to get supported axes. Error:" << ::strerror( errno );
			continue;
		}

		// device should support at least buttons 'A' or '1' or 'TRIGGER'
		if ( !Tools::testBit( BTN_TRIGGER, keys ) && !Tools::testBit( BTN_A, keys ) && !Tools::testBit( BTN_1, keys ) )
		{
			GRIM_DEBUG << "Gamepad does not support buttons 'A' or '1' or 'TRIGGER'";
			continue;
		}

		// device should support at least axes X and Y
		if ( !Tools::testBit( ABS_X, axes ) || !Tools::testBit( ABS_Y, axes ) )
		{
			GRIM_DEBUG << "Device does not support axes X and Y";
			continue;
		}

		// device is ok
		Device * const device = new Device( deviceIdGenerator_.take(), manager_ );
		DevicePrivate * const devicePrivate = device->d_;

		devicePrivate->filePath_ = filePath;
		devicePrivate->fileHandle_ = ::dup( file.handle() );
		file.close();

		// get the device name
		char nameBuffer[128];
		if ( ::ioctl( devicePrivate->fileHandle_, EVIOCGNAME( sizeof(nameBuffer) ), nameBuffer ) < 0 )
		{
			GRIM_DEBUG << "Failed to get device name, use file name instead";
			devicePrivate->name_ = fileName;
		}
		else
		{
			devicePrivate->name_ = QString::fromLocal8Bit( nameBuffer, sizeof(nameBuffer)-1 );
		}
		GRIM_DEBUG << "Name:" << devicePrivate->name_;

		// get the device manufacturer and product IDs
		::input_id id;
		if ( ::ioctl( devicePrivate->fileHandle_, EVIOCGID, &id ) == 0 )
		{
			devicePrivate->vendorId_ = id.vendor;
			devicePrivate->productId_ = id.product;
		}

		// init buttons
		for ( int buttonIndex = BTN_MISC; buttonIndex < KEY_CNT; ++buttonIndex )
		{
			if ( !Tools::testBit( buttonIndex, keys ) )
				continue;

			GRIM_DEBUG << "button:" << Debug::Log::hex( buttonIndex );

			const ButtonType buttonType = buttonTypeForKey( buttonIndex );
			if ( buttonType == Button_Null )
				continue;

			ButtonInfo & buttonInfo = devicePrivate->buttonInfos_[ buttonType ];
			FLAT_ASSERT(!buttonInfo.isValid);

			buttonInfo.isValid = true;
		}

		// init axes
		for ( int axisIndex = 0; axisIndex < ABS_CNT; ++axisIndex )
		{
			if ( !Tools::testBit( axisIndex, axes ) )
				continue;

			const AxisType axisType = axisTypeForAbs( axisIndex );
			if ( axisType == Axis_Null )
				continue;

			AxisInfo & axisInfo = devicePrivate->axisInfos_[ axisType ];
			FLAT_ASSERT(!axisInfo.isValid);

			::input_absinfo axis;
			if ( ::ioctl( devicePrivate->fileHandle_, EVIOCGABS( axisIndex ), &axis ) < 0 )
				continue;

			GRIM_DEBUG << "axis" << Debug::Log::hex( axisIndex ) << Debug::Log::NoSpaceOnce() << ":";
			GRIM_DEBUG << "  value =" << axis.value;
			GRIM_DEBUG << "  min   =" << axis.minimum;
			GRIM_DEBUG << "  max   =" << axis.maximum;
			GRIM_DEBUG << "  fuzz  =" << axis.fuzz;
			GRIM_DEBUG << "  flat  =" << axis.flat;
			GRIM_DEBUG << "  res   =" << axis.resolution;

			if ( axis.minimum >= axis.maximum )
				continue;

			axisInfo.minValue = axis.minimum;
			axisInfo.maxValue = axis.maximum;
			axisInfo.value = axis.value;
			axisInfo.range = axisInfo.maxValue - axisInfo.minValue;
			axisInfo.isValid = true;
		}

		devicePrivate->_createFileWatcher();

		_deviceAttached( device );
	}
}
#endif




}}
